package com.company;

import java.util.Comparator;

class NameComparator implements Comparator{
    @Override
    public int compare(Object o1, Object o2){
        return ((Book) o1).name.compareTo(((Book) o2).name);
    }
}
class SurnameComparator implements Comparator{
    @Override
    public int compare(Object o1, Object o2){
        return ((Book) o1).surname.compareTo(((Book) o2).surname);
    }
}
