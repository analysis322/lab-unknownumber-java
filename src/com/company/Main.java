package com.company;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Alexander Shiganov on 21.10.17.
 */
public class Main {

    static String books_file = "books.txt", readers_file = "readers.txt", return_books_file = "return_books.txt";
    public static ArrayList<Book> book_list = new ArrayList<>();
    public static ArrayList<Reader> reader_list = new ArrayList<>();
    public static ArrayList<CounterReturn> return_list = new ArrayList<>();

    public static void main(String[] args) throws IOException {
	    Scanner sc = new Scanner(System.in);
        book_list = FileReader.getBook(books_file);
        reader_list = FileReader.getReader(readers_file);
        return_list = FileReader.getCounterReturn(return_books_file);
        System.out.println("Hello=)");
        while(true){
            System.out.println();
            System.out.println("1 - Add a book or reader.");
            System.out.println("2 - Delete a book or reader.");
            System.out.println("3 - Choosing a book for the reader.");
            System.out.println("4 - Issuing the book to the reader.");
            System.out.println("5 - Return of the book.");
            System.out.println("6 - List of books, author's surname.");
            System.out.println("7 - List of books by name.");
            System.out.println("8 - List of readers");
            System.out.println("9 - List of books");
            System.out.println("10 - Exit.");
            System.out.println("11 - List of books which is not available in the library(test).");
            int sw = 0;
            try{
                sw = sc.nextInt();
            }
            catch (InputMismatchException e){
                System.out.println("Oops, something's wrong. Re-enter =/");
            }
            sc.nextLine();
            switch (sw){
                case 1:
                    System.out.println("1 - Add a book.");
                    System.out.println("2 - Add reader.");
                    try{
                        sw = sc.nextInt();
                    }
                    catch (InputMismatchException e){
                        System.out.println("Oops, something's wrong. Re-enter =/");
                        break;
                    }
                    sc.nextLine();
                    if(sw == 1){
                        System.out.println("Create a book...");
                        System.out.println("Enter id.");
                        int id = 0;
                        try{
                            id = sc.nextInt();
                        }catch (InputMismatchException e){
                            System.out.println("Oops, something's wrong. Re-enter =/");
                            break;
                        }
                        sc.nextLine();
                        System.out.println("Enter surname author.");
                        String surname = sc.nextLine();
                        System.out.println("Enter initials.");
                        String initials = sc.nextLine();
                        System.out.println("Enter name of book.");
                        String name = sc.nextLine();
                        System.out.println("Enter genre.");
                        String ganre = sc.nextLine();
                        System.out.println("Enter number of books.");
                        int number = 0;
                        try{
                            number = sc.nextInt();
                        }catch (InputMismatchException e){
                            System.out.println("Oops, something's wrong. Re-enter =/");
                            break;
                        }
                        sc.nextLine();
                        int age = 0;
                        try{
                            number = sc.nextInt();
                        }catch (InputMismatchException e){
                            System.out.println("Oops, something's wrong. Re-enter =/");
                            break;
                        }
                        sc.nextLine();
                        Book book = new Book(id, surname, initials, name, ganre, age, number);
                        System.out.println("You create:"+book.toString());
                        book_list.add(book);
                    }
                    else{
                        System.out.println("create reader");
                        System.out.println("Enter surname");
                        String surname = sc.nextLine();
                        System.out.println("Enter name");
                        String name = sc.nextLine();
                        System.out.println("Enter patronymic");
                        String patr = sc.nextLine();
                        System.out.println("Enter age");
                        int age = 0;
                        try{
                            age = sc.nextInt();
                        }catch (InputMismatchException e){
                            System.out.println("Oops, something's wrong. Re-enter =/");
                            break;
                        }
                        System.out.println("created reader");
                        Reader reader = new Reader(return_list.size(),surname,name,patr,age);
                        reader_list.add(reader);
                    }
                    break;
                case 2:
                    System.out.println("1 - Delete a book.");
                    System.out.println("2 - Delete reader.");
                    try{
                        sw = sc.nextInt();
                    }
                    catch (InputMismatchException e){
                        System.out.println("Oops, something's wrong. Re-enter =/");
                        break;
                    }
                    sc.nextLine();
                    if(sw == 1){
                        System.out.println("Delete a book");
                        System.out.println("Enter a book id");
                        int id = 0;
                        try{
                            id = sc.nextInt();
                        }
                        catch (InputMismatchException e){
                            System.out.println("Oops, something's wrong. Re-enter =/");
                            break;
                        }
                        for(int i=0; i<book_list.size();i++){
                            if(id == book_list.get(i).getId())
                                book_list.remove(i);
                        }
                    }
                    else{
                        System.out.println("Delete reader");
                        System.out.println("Enter the reader's surname");
                        String surname = sc.nextLine();
                        for(int i=0; i<reader_list.size();i++){
                            if(true == surname.equals(reader_list.get(i).getSurname()))
                                reader_list.remove(i);
                        }
                    }
                    break;
                case 3:
                    // I don't understand how to use the age of the reader. More precisely, why is it needed?
                    // That's why I'm not using age here. kek kul'mek =)
                    // UPD: you need to fix this! I'm just too lazy=)
                    System.out.println("1 - Choose a book by genre");
                    System.out.println("2 - Choose a book by surname");
                    try{
                        sw = sc.nextInt();
                    }
                    catch (InputMismatchException e){
                        System.out.println("Oops, something's wrong. Re-enter =/");
                        break;
                    }
                    sc.nextLine();
                    if(sw == 1){
                        System.out.println("Enter genre");
                        String genre = sc.nextLine();
                        for(int i=0; i<book_list.size();i++){
                            if(genre.equals(book_list.get(i).getGenre()))
                                System.out.println(book_list.get(i).toString());
                        }
                    }
                    else{
                        System.out.println("Enter surname");
                        String surname = sc.nextLine();
                        for(int i=0; i<book_list.size();i++){
                            if(surname.equals(book_list.get(i).getSurname()))
                                System.out.println(book_list.get(i).toString());
                        }
                    }
                    break;
                case 4:
                    //There is already a need to create a complete database,
                    //because the books that are issued cannot be stored in a txt file!
                    //Or I'm just stupid=)
                    //Well, I'll make a simulator database
                    //Can be done prettier, but I'm again too lazy
                    System.out.println("Enter the reader id");
                    int id_reader;
                    try{
                        id_reader = sc.nextInt();
                    }
                    catch (InputMismatchException e){
                        System.out.println("Oops, something's wrong. Re-enter =/");
                        break;
                    }
                    sc.nextLine();
                    System.out.println("Enter the id of the book");
                    int id_book;
                    try{
                        id_book = sc.nextInt();
                    }
                    catch (InputMismatchException e){
                        System.out.println("Oops, something's wrong. Re-enter =/");
                        break;
                    }
                    sc.nextLine();
                    System.out.println("Enter the return date in the format yyyy-MM-dd");
                    String date = sc.nextLine();
                    SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");
                    Date parsingDate = null;
                    try {
                        parsingDate = ft.parse(date);
                        System.out.println(parsingDate);
                    }catch (ParseException e) {
                        System.out.println("Not analyzed with the help of " + ft);
                        break;
                    }
                    boolean flag = false;
                    for(int i=0; i<book_list.size();i++){
                        if(book_list.get(i).id==id_book && book_list.get(i).number_books > 0){
                            book_list.get(i).number_books--;
                            flag = true;
                        }
                    }
                    if(flag) {
                        CounterReturn ret = new CounterReturn(id_reader, id_book, parsingDate);
                        System.out.println("The return book - " + ret.toString());
                        return_list.add(ret);
                    }else
                        System.out.println("No book in the library");
                    break;
                case 5:
                    System.out.println("Enter the reader id");
                    try{
                        id_reader = sc.nextInt();
                    }
                    catch (InputMismatchException e){
                        System.out.println("Oops, something's wrong. Re-enter =/");
                        break;
                    }
                    sc.nextLine();
                    System.out.println("Enter the id of the book");
                    try{
                        id_book = sc.nextInt();
                    }
                    catch (InputMismatchException e){
                        System.out.println("Oops, something's wrong. Re-enter =/");
                        break;
                    }
                    sc.nextLine();
                    for(int i=0; i<return_list.size();i++){
                        if(return_list.get(i).id_reader == id_reader && return_list.get(i).id_book == id_book){
                            Date dateNow = new Date();
                            long difference = dateNow.getTime() - return_list.get(i).date.getTime();
                            // Converting the number of days between the date from milliseconds to days
                            int days =  (int)(difference / (24 * 60 * 60 * 1000)); // milliseconds / (24h * 60min * 60sec * 1000 ms)
                            if(days>0) {
                                System.out.println("Debt:" + days + " day");
                                System.out.println("You have to pay:" + days * 1);
                            }
                            for(int j=0; j<book_list.size();j++){
                                if(book_list.get(j).id==return_list.get(i).id_book)
                                    book_list.get(i).number_books++;
                            }
                            return_list.remove(i);
                        }
                    }
                    break;
                case 6:
                    Collections.sort(book_list, new SurnameComparator());
                    System.out.println("List of book:\nid-surname-initials-book name-genre-age-number books");
                    for(int i=0; i<book_list.size();i++){
                        System.out.println(book_list.get(i).toString());
                    }
                    break;
                case 7:
                    Collections.sort(book_list, new NameComparator());
                    System.out.println("List of book:\nid-surname-initials-book name-genre-age-number books");
                    for(int i=0; i<book_list.size();i++){
                        System.out.println(book_list.get(i).toString());
                    }
                    break;
                case 8:
                    System.out.println("List of reader:\nsuename-name-patronymic-age");
                    for(int i=0; i<reader_list.size();i++){
                        System.out.println(reader_list.get(i).toString());
                    }
                    break;
                case 9:
                    System.out.println("List of book:\nid-suname-initials-book name-genre-age-number books");
                    for(int i=0; i<book_list.size();i++){
                        System.out.println(book_list.get(i).toString());
                    }
                    break;
                case 10:
                    System.out.println("Good bay=)");
                    FileReader.writeBook(books_file,book_list);
                    FileReader.writeReader(readers_file,reader_list);
                    FileReader.writeReturnBooks(return_books_file,return_list);
                    return;
                case 11:
                    for(int i=0;i<return_list.size();i++)
                        System.out.println(return_list.get(i).toString());
                    break;
            }
        }
    }
}
