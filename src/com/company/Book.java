package com.company;

/**
 * Created by Alexander Shiganov on 21.10.17.
 */
public class Book {
    String surname, initials, name, genre;
    int id, number_books, age;
    public Book(int id, String surname, String initials, String name, String genre, int age, int number_books ){
        this.id = id;
        this.surname = surname;
        this.initials = initials;
        this.name = name;
        this.genre = genre;
        this.age = age;
        this.number_books = number_books;
    }
    public String toString(){
        return id+" "+surname+" "+initials+" "+name+" "+genre+" "+age+" "+number_books;
    }
    public int getId(){
        return id;
    }
    public String getGenre(){
        return genre;
    }
    public String getSurname(){
        return surname;
    }
}
