package com.company;

import java.text.SimpleDateFormat;
import java.util.Date;

public class CounterReturn {
    int id_reader, id_book;
    Date date;
    CounterReturn(int id_reader, int id_book, Date date){
        this.id_reader = id_reader;
        this.id_book = id_book;
        this.date = date;
    }

    @Override
    public String toString() {
        SimpleDateFormat formatForDate = new SimpleDateFormat("yyyy-MM-dd");
        return id_reader+" "+id_book+" "+formatForDate.format(date);
    }
}
