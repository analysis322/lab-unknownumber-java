package com.company;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static java.lang.Integer.parseInt;

/**
 * Created by Alexander Shiganov on 21.10.17.
 */
public class FileReader {
    private static void exists(String file_name) throws FileNotFoundException {
        java.io.File file = new java.io.File(file_name);
        if(!file.exists()){
            throw new FileNotFoundException(file.getName());
        }
    }

    public static ArrayList<Reader> getReader(String file_name) throws IOException {
        ArrayList<Reader> readers = new ArrayList<>();
        exists(file_name);
        java.io.File file = new java.io.File(file_name);
        try{
            BufferedReader in = new BufferedReader(new java.io.FileReader(file.getAbsoluteFile()));
            try{
                String s;
                while((s = in.readLine()) != null) {
                    try {
                        String[] buffer = s.split(" ");
                        Reader reader = new Reader(parseInt(buffer[0]), buffer[1], buffer[2], buffer[3], parseInt(buffer[4]));
                        readers.add(reader);
                    }catch (Exception e){
                        System.out.println("Oops! Error reading file! Invalid value missing");
                    }
                }
            }finally {
                in.close();
            }
        }catch (IOException e){
            throw new RuntimeException(e);
        }
        return readers;
    }

    public static ArrayList<Book> getBook(String file_name) throws IOException {
        ArrayList<Book> books = new ArrayList<>();
        exists(file_name);
        java.io.File file = new java.io.File(file_name);
        try{
            BufferedReader in = new BufferedReader(new java.io.FileReader(file.getAbsoluteFile()));
            try{
                String s;
                while((s = in.readLine()) != null) {
                    try {
                        String[] buffer = s.split(" ");
                        Book book = new Book(parseInt(buffer[0]), buffer[1], buffer[2], buffer[3], buffer[4], parseInt(buffer[5]), parseInt(buffer[6]));
                        books.add(book);
                    }catch (Exception e){
                        System.out.println("Oops! Error reading file! Invalid value missing");
                    }
                }
            }finally {
                in.close();
            }
        }catch (IOException e){
            throw new RuntimeException(e);
        }
        return books;
    }

    public static ArrayList<CounterReturn> getCounterReturn(String file_name) throws IOException {
        ArrayList<CounterReturn> count_ret = new ArrayList<>();
        exists(file_name);
        java.io.File file = new java.io.File(file_name);
        try{
            BufferedReader in = new BufferedReader(new java.io.FileReader(file.getAbsoluteFile()));
            try{
                String s;
                while((s = in.readLine()) != null) {
                    try {
                        String[] buffer = s.split(" ");
                        SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");
                        CounterReturn ret = new CounterReturn(parseInt(buffer[0]), parseInt(buffer[1]), ft.parse(buffer[2]));
                        count_ret.add(ret);
                    }catch (Exception e){
                        System.out.println("Oops! Error reading file! Invalid value missing");
                    }
                }
            }finally {
                in.close();
            }
        }catch (IOException e){
            throw new RuntimeException(e);
        }
        return count_ret;
    }

    public static void writeReader(String file_name, ArrayList<Reader> list){
        java.io.File file = new java.io.File(file_name);
        try{
            if(file.exists()){
                file.createNewFile();
            }
            PrintWriter out = new PrintWriter(file.getAbsoluteFile());
            try{
                for(int i=0; i<list.size();i++){
                    out.println(list.get(i).toString());
                }
            }finally {
                out.close();
            }
        }catch (IOException e){
            throw new RuntimeException(e);
        }
    }

    public static void writeBook(String file_name, ArrayList<Book> list){
        java.io.File file = new java.io.File(file_name);
        try{
            if(file.exists()){
                file.createNewFile();
            }
            PrintWriter out = new PrintWriter(file.getAbsoluteFile());
            try{
                for(int i=0; i<list.size();i++){
                    out.println(list.get(i).toString());
                }
            }finally {
                out.close();
            }
        }catch (IOException e){
            throw new RuntimeException(e);
        }
    }

    public static void writeReturnBooks(String file_name, ArrayList<CounterReturn> list){
        java.io.File file = new java.io.File(file_name);
        try{
            if(file.exists()){
                file.createNewFile();
            }
            PrintWriter out = new PrintWriter(file.getAbsoluteFile());
            try{
                for(int i=0; i<list.size();i++){
                    out.println(list.get(i).toString());
                }
            }finally {
                out.close();
            }
        }catch (IOException e){
            throw new RuntimeException(e);
        }
    }

}
