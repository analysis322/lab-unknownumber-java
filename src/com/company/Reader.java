package com.company;

/**
 * Created by Alexander Shiganov on 21.10.17.
 */
public class Reader {
    String surname, name, patronymic;
    int age,id;

    Reader(int id,String surname, String name, String patronymic, int age){
        this.id = id;
        this.surname = surname;
        this.name = name;
        this.patronymic = patronymic;
        this.age = age;
    }
    public String toString(){
        return id+" "+surname+" "+name+" "+patronymic+" "+age;
    }
    public String getSurname(){
        return surname;
    }
}
